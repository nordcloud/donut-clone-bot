'use strict'

import middy from '@middy/core'
import cors from '@middy/http-cors'
import axios from 'axios';
import { defaultJson, boomErrorHandler } from 'slack_bot_lib/middleware';
import { SlackUtil } from 'slack_bot_lib/utils/slack-util';
import AWS from "aws-sdk";

const sqs = new AWS.SQS({apiVersion: '2012-11-05'});

// This chunk a big array to multiple arrays
// sorry for the recursive function ¯\_(ツ)_/¯
const chunkArray = (array, maxPerSubArray) => {
  if(!array) return []; //safe check

  const firstChunk = array.slice(0, maxPerSubArray);
  if(firstChunk.length == 0) return []; // base condition to break recursive call

  const remaining = array.slice(maxPerSubArray, array.length)

  return [[...firstChunk], ...chunkArray(remaining, maxPerSubArray)];
} 

const shuffleArray = a => {
  for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [a[i], a[j]] = [a[j], a[i]];
  }
  return a;
}

const handler = async event => {
  try {
    let delayInSecond = 0;
    const util = new SlackUtil(process.env.slackToken);
     const slackUsers = await util.fetchUsersFromChannel('XXXXXXXXXX');
    const shuffledUsers = shuffleArray(slackUsers);

    // chunk array so only 1 group of 4 user appears in the message queue every 10 seconds
    // not to overheat the Slack API 
    const chunkedSlackUsers = chunkArray(shuffledUsers, 4);
    chunkedSlackUsers.forEach(async chunk => {
      const params = {
        QueueUrl: process.env.sqsURL,
        MessageBody: JSON.stringify ({
           userIds: [...chunk],
        }),
        DelaySeconds: delayInSecond
      }
      // delay 10 second, TODO: delay in second only maximum 900 seconds. NEED to find more efficient way
      delayInSecond = delayInSecond + 10; 
      await sqs.sendMessage(params).promise();
    });
  }
  catch(e) {
    console.log(e)
    return new Error(e)
  }
}

const lambda = middy(handler)
  .use(cors())
  .use(defaultJson())
  .use(boomErrorHandler)

export { lambda as handler }