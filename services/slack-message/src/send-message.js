'use strict'

import middy from '@middy/core'
import cors from '@middy/http-cors'
import { defaultJson, boomErrorHandler } from 'slack_bot_lib/middleware';
import { SlackUtil } from 'slack_bot_lib/utils/slack-util';

const message = `:wave: I'm here to help you get to know your teammates by pairing everyone from #virtual-coffee every Wed.


Now that you're here, let's schedule a time to meet for coffee :coffee:, lunch :sandwich: or donut :doughnut: (Or via computer :computer: if you're in different locations !!!) 


:warning: this is a quick in-house solution, so the only way to stop pairing for now is to leave #virtual-coffee`

const handler = async event => {
  try {
    const util = new SlackUtil(process.env.slackToken);

    const { Records } = event;
    Records.forEach(async record => {
      const data = JSON.parse(record.body);
      const groupChatId  = await util.createGroupMessage(data.userIds)
      util.sendSlackMessage(groupChatId, message)
    });
  }
  catch(e) {
    console.log(e)
    throw new Error(e)
  }
}

const lambda = middy(handler)
  .use(cors())
  .use(defaultJson())
  .use(boomErrorHandler)

export { lambda as handler }