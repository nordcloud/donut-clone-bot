'use strict'

const axios = require('axios');
const crypto = require('crypto');

class SlackUtil {
  constructor(slackToken) {
    this.token = slackToken
  }

  async fetchUsersFromChannel(channelId) {
    const response = await axios.get(`https://slack.com/api/conversations.members?channel=${channelId}`, {
      headers: {
          Authorization: `Bearer ${this.token}`,
          'Content-Type': 'application/json',
          }
        }
      )
    const { members } = response.data;
    return members;
  }

  async createGroupMessage(userIds) {
    let userIdString = userIds.join(',');
    const response = await axios.post('https://slack.com/api/conversations.open',
      {
        users: userIdString,
      }
      ,{
      headers: {
        Authorization: `Bearer ${this.token}`,
        'Content-Type': 'application/json',
      }
    });
    const { channel } = response.data;
    return channel.id;
  }

  async sendSlackMessage(channelId, text) {
      const response = await axios.post('https://slack.com/api/chat.postMessage',
        {
          channel: channelId,
          text: text
        }
        ,{
        headers: {
          Authorization: `Bearer ${this.token}`,
          'Content-Type': 'application/json',
        }
      });
  }
}

module.exports = { SlackUtil }
