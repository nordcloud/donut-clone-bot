#! /usr/bin/env bash

if [ "$1" != "" ]; then
    ENV=$1
else
    echo "Please add an environment to deploy (dev,testing,production)"
    exit 0
fi

token="XXXXX"

aws ssm put-parameter --cli-input-json "{\"Name\": \"/${ENV}/slacktoken\", \"Value\": \"${token}\", \"Type\": \"SecureString\"}" --overwrite --region eu-west-1
