This is a repo for Nordcloud's donut match bot. 
The project include:
   * a lambda that being trigger by CloudWatch events (`push.js`), that fetch the list of users belong to the channel and then split it to a group of 4 and then push them to the SQS queue

   * a lambda that listenn to SQS queue, pull the queue, open the conversation between peoples and send an initial message (`send-message.js`)

Architect:

![Alt text](./architect.png?raw=true "Architect")

# HOW TO SET UP THE BOT FOR NEW CHANNEL (SIMPLE WAY) #
* 1) Find the channel Id. Easiest way is to follow this (https://www.wikihow.com/Find-a-Channel-ID-on-Slack-on-PC-or-Mac)
* 2) Then replace the ID to services/slack-message/push.js in the call to function `fetchUsersFromChannel(<channel-id>)`, for example: `C0104E3P19V -- virtual-coffe channel`
* 3) Ask to be added to the bot_health_check Slack app
* 4) The app need an SSM ( Secret management ) for storing Slack token
   Hence, go to slack dashboard ( https://api.slack.com/apps/A010PN3LEES/oauth ) and copy the Bot User OAuth Access Token
   Open the `ssm-put.sh` and replace the `token="XXXXX"` with `token=<Slack_Tokenn>`
   Run the `ssm-put.sh` to deploy SSM to your aws accounnt
* 5) cd to `services/slack-message` and run `sls deploy <your-aws-profile>`


# NOTE / Catchas#
Cloud watch events (CRON job) has some nasty retry behavior that could spam people .
Remember to change a retry attemp in the push-message-to-queue to 0 to prevent spamming when the function failed
Currently(28/04/2019) there is no way to set it with Serverless framework
![Alt text](./catcha1.png?raw=true "Catcha 1")


Brief project structure below:   
```
/ - root folder
│
└───lib - shared library between services within project
│    │
│    └───dynamodb - dynamoDB mapper model and table classes
│    │
│    └───docs - Swagger and related docs
│    │
│    └───schemas - Joi Schema files
│    │
│    └───utils
│
└───services - Code for APIs
│    │
│    └───service 1
│    │    │
│    │    └───src
│    │    │     └───get.js
│    │    │     └───list.js
│    │    │     └───etc...
│    │    │
│    │    └───package.json
│    │    │
│    │    └───servless.yaml (each service is a separate CF stack)
│    │    │
│    │    └───webpack.config.js
│    │
│    └───service 2
│
└───tests - Tests for services or utils
```